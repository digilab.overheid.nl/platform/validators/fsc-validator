package main

import (
	"fmt"
	"os"
	"path/filepath"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/certutil"

	"github.com/alexflint/go-arg"
)

var args struct {
	Path string `arg:"" help:"path to scan" default:"./config/pki"`
}

func main() {
	arg.MustParse(&args)
	ScanFingerprints(args.Path)
}

func ScanFingerprints(path string) {

	files, err := WalkMatch(path, "*.pem")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		cert, err := certutil.LoadX509Cert(file)
		if err != nil {
			// fmt.Println("-> Loadcert failed", err)
			continue
		}
		fp := certutil.CertificateFingerprint(&cert)

		fmt.Printf("FP `%s` for %s (%s)\n", fp, file, cert.Subject.SerialNumber)
	}
}
func WalkMatch(root, pattern string) ([]string, error) {
	var matches []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if matched, err := filepath.Match(pattern, filepath.Base(path)); err != nil {
			return err
		} else if matched {
			matches = append(matches, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return matches, nil
}
