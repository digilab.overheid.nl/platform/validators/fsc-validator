package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"
)

func main() {

	port := flag.Int("port", 11111, "port for server to listen on")
	name := flag.String("name", "Test", "serice name to use")

	flag.Parse()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("GOT REQUEST: %v\n", r.URL.Path)

		for key, val := range r.Header {
			if strings.HasPrefix(key, "X-") || strings.HasPrefix(key, "Fsc-") {
				log.Printf("\tH: %v %v\n", key, val)
			}
		}

		req_service := r.Header["X-Nlx-Service"][0]

		if req_service == "" {
			fmt.Fprintf(w, "You have reached `%s` but your service header is missing!", *name)
		} else if req_service == *name {
			fmt.Fprintf(w, "Hello from `%s`!", *name)
		} else {
			fmt.Fprintf(w, "You have reached `%s` instead of `%s`!", *name, req_service)
		}
	})

	fmt.Printf("Starting server at port %d\n", *port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", *port), nil); err != nil {
		log.Fatal(err)
	}
}
