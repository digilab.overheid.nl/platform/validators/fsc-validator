package outway

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	"digilab.network/gopkg/fsc-validator/fsc-outway/pkg/certgroup"
	"digilab.network/gopkg/fsc-validator/fsc-outway/pkg/config"
)

func SendTokenString(cfg *config.Config, tokenString string) error {
	orgCertGroup, err := certgroup.NewOrgCertGroup(cfg.OutwayTlsCa, cfg.OutwayTlsCert, cfg.OutwayTlsKey)
	if err != nil {
		return errors.Wrap(err, "could not get tls keypair")
	}

	client, err := buildTlsClient(orgCertGroup)
	if err != nil {
		return errors.Wrap(err, "could not build http client")
	}

	err = sendRequest(client, cfg, tokenString)
	if err != nil {
		return errors.Wrap(err, "Could not send request: ")
	}

	return nil
}

func buildTlsClient(certGroup *certgroup.OrgCertGroup) (*http.Client, error) {
	client := &http.Client{Transport: &http.Transport{TLSClientConfig: &tls.Config{
		RootCAs:      &certGroup.OrgCas,
		Certificates: []tls.Certificate{certGroup.OrgCert},
	}}}

	return client, nil
}

func sendRequest(client *http.Client, cfg *config.Config, tokenString string) error {
	req, err := http.NewRequest("GET", cfg.InwayUrl, nil)
	if err != nil {
		return errors.Wrap(err, "failed to build")
	}

	req.Header.Set("fsc-authorization", tokenString)
	req.Header.Set("fsc-grant-hash", cfg.GrantHash)
	req.Header.Set("fsc-transaction-id", uuid.New().String())

	res, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to send")
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response body")
	}

	log.Println("Status code: ", res.StatusCode)
	log.Printf("client: response body: %s\n", resBody)

	return nil
}
