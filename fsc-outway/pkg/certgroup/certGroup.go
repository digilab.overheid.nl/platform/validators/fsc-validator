package certgroup

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"os"

	"github.com/pkg/errors"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/certutil"
)

type OrgCertGroup struct {
	OrgCertificateFingerprint string
	OrgCas                    x509.CertPool
	OrgCert                   tls.Certificate
}

func NewOrgCertGroup(ca_file, cert_file, key_file string) (*OrgCertGroup, error) {
	// Load cas
	caCert, err := os.ReadFile(ca_file)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read ca cert")
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// load outway key cert pair, and get fingerprint
	org_cert, err := tls.LoadX509KeyPair(cert_file, key_file)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load x509 keypair")
	}

	leaf, err := x509.ParseCertificate(org_cert.Certificate[0])
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse leaf")
	}
	org_fp := certutil.CertificateFingerprint(leaf)

	certGroup := OrgCertGroup{
		OrgCertificateFingerprint: org_fp,
		OrgCas:                    *caCertPool,
		OrgCert:                   org_cert,
	}
	return &certGroup, nil
}

func (c OrgCertGroup) GetRsaKey() (*rsa.PrivateKey, error) {
	var rsaKey *rsa.PrivateKey
	if key, ok := c.OrgCert.PrivateKey.(*rsa.PrivateKey); ok {
		rsaKey = key
	} else {
		return nil, errors.New("private key is not a valid rsa key")
	}

	return rsaKey, nil
}
