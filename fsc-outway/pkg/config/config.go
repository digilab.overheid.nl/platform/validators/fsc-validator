package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/pkg/errors"
)

type Config struct {
	OutwayTlsCa   string `json:"outway_tls_ca,omitempty"`
	OutwayTlsCert string `json:"outway_tls_cert,omitempty"`
	OutwayTlsKey  string `json:"outway_tls_key,omitempty"`

	GrantHash string `json:"grant_hash,omitempty"`
	InwayUrl  string `json:"inway_url,omitempty"`
}

func ReadConfig(configFile string) (*Config, error) {
	configData, err := os.ReadFile(configFile)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read config file")
	}

	var config Config
	err = json.Unmarshal(configData, &config)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse config file")
	}

	return &config, nil
}

func (c *Config) IsValidForSend() bool {
	validationErrors := c.validForSend()
	err := formatValidationError(validationErrors)
	if err != nil {
		log.Println(err)
		return false
	} else {
		return true
	}

}

func (c *Config) validForSend() []string {
	validationErrors := make([]string, 0, 10)
	if c.GrantHash == "" {
		validationErrors = append(validationErrors, "grant_hash")
	}
	if c.OutwayTlsCert == "" {
		validationErrors = append(validationErrors, "outway_tls_cert")
	}
	if c.InwayUrl == "" {
		validationErrors = append(validationErrors, "inway_url")

	}
	if c.OutwayTlsCa == "" {
		validationErrors = append(validationErrors, "outway_tls_ca")

	}
	if c.OutwayTlsKey == "" {
		validationErrors = append(validationErrors, "outway_tls_key")
	}

	return validationErrors
}

func formatValidationError(errs []string) error {

	if len(errs) == 0 {
		return nil
	} else {
		errorMessage := fmt.Sprintln("Configuration is missing the following items: ", strings.Join(errs, ","))
		return errors.New(errorMessage)
	}
}
