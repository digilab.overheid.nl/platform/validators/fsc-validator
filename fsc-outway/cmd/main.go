package main

import (
	"log"

	"digilab.network/gopkg/fsc-validator/fsc-outway/pkg/config"
	"digilab.network/gopkg/fsc-validator/fsc-outway/pkg/outway"
	"github.com/alexflint/go-arg"
)

var args struct {
	Config      string `arg:"required" help:"path to configuration file"`
	TokenString string `arg:"required" help:"tokenstring to send to outway"`
}

func main() {
	arg.MustParse(&args)

	configFile := args.Config
	tokenString := args.TokenString

	cfg, err := config.ReadConfig(configFile)
	if err != nil {
		log.Fatalln(err)
	}

	if !cfg.IsValidForSend() {
		return
	}

	err = outway.SendTokenString(cfg, tokenString)
	if err != nil {
		log.Fatalln(err)
	}

}
