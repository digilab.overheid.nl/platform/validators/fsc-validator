module digilab.network/gopkg/fsc-validator/fsc-outway

go 1.20

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
)

require github.com/alexflint/go-scalar v1.1.0 // indirect
