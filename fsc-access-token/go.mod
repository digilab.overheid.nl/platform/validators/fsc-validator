module digilab.network/gopkg/fsc-validator/fsc-access-token

go 1.20

require github.com/alexflint/go-scalar v1.1.0 // indirect

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/pkg/errors v0.9.1
)
