package accesstoken

import (
	"log"
	"time"

	jwt "github.com/golang-jwt/jwt/v5"
	"github.com/pkg/errors"

	"digilab.network/gopkg/fsc-validator/fsc-access-token/pkg/certgroup"
	"digilab.network/gopkg/fsc-validator/fsc-access-token/pkg/config"
	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/jsontoken"
)

func CreateTokenString(cfg *config.Config) (string, error) {

	tokenCertGroup, err := certgroup.NewTokenCertGroup(cfg.OutwayTlsCert, cfg.TokenSignCert, cfg.TokenSignKey, cfg.SignatureAlgorithmType())
	if err != nil {
		return "", errors.Wrap(err, "could not get tokencerts")
	}

	claims := buildClaims(cfg, tokenCertGroup)

	tokenString, err := buildToken(cfg, claims, tokenCertGroup)
	if err != nil {
		return "", errors.Wrap(err, "could not build token")
	}

	return tokenString, nil
}

func buildClaims(cfg *config.Config, certGroup *certgroup.TokenCertGroup) jsontoken.JsonToken {
	now := time.Now().Unix()
	exp := time.Now().Add(time.Second * time.Duration(*cfg.TokenLifetime)).Unix()

	return jsontoken.JsonToken{
		GrantHash:             cfg.GrantHash,
		OutwayPeerID:          cfg.OutwayPeerID,
		OutwayDelegatorPeerID: "",
		ServiceName:           cfg.ServiceName,
		ServiceInwayAddress:   cfg.ServiceAddress,
		ServicePeerID:         cfg.InwayPeerID,
		ServiceDelegatorID:    "",
		ExpiryDate:            exp,
		NotBefore:             now,
		ConfirmationMethod: jsontoken.ConfirmationMethod{
			Fingerprint: certGroup.OrgCertificateFingerprint,
		},
	}
}

func buildToken(cfg *config.Config, claims jsontoken.JsonToken, certGroup *certgroup.TokenCertGroup) (string, error) {

	var signingMethod jwt.SigningMethod

	switch cfg.SignatureAlgorithm {
	case "RS256":
		signingMethod = jwt.SigningMethodRS256
	case "RS384":
		signingMethod = jwt.SigningMethodRS384
	case "RS512":
		signingMethod = jwt.SigningMethodRS512
	case "ES256":
		signingMethod = jwt.SigningMethodES256
	case "ES384":
		signingMethod = jwt.SigningMethodES384
	case "ES512":
		signingMethod = jwt.SigningMethodES512
	default:
		log.Fatalln("unrecognized signature algorithm")
	}

	token := jwt.NewWithClaims(signingMethod, claims)
	token.Header["x5t#S256"] = certGroup.TokenCertificateFingerprint

	key, err := certGroup.GetRsaKey()
	if err != nil {
		return "", errors.Wrap(err, "failed to get rsa key")
	}

	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", errors.Wrap(err, "failed to sign token")
	}

	return tokenString, nil
}
