package certgroup

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"

	"github.com/pkg/errors"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/certutil"
)

type TokenCertGroup struct {
	OrgCertificateFingerprint   string
	TokenCertificateFingerprint string
	TokenCert                   tls.Certificate
}

func NewTokenCertGroup(org_cert_file, cert_file, key_file, signature_algorithm_type string) (*TokenCertGroup, error) {
	// load token key cert pair, and get fingerprint
	tkn_cert, err := tls.LoadX509KeyPair(cert_file, key_file)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load token x509 keypair")
	}

	leaf, err := x509.ParseCertificate(tkn_cert.Certificate[0])
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse token leaf")
	}
	if leaf.PublicKeyAlgorithm.String() != signature_algorithm_type {
		return nil, errors.New("certificate does not match configured signature algorithm")
	}
	tkn_fp := certutil.CertificateFingerprint(leaf)

	// Load org cert and get fingerprint
	org_cert, err := certutil.LoadX509Cert(org_cert_file)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load org x509 cert")
	}
	org_fp := certutil.CertificateFingerprint(&org_cert)

	certGroup := TokenCertGroup{
		OrgCertificateFingerprint:   org_fp,
		TokenCertificateFingerprint: tkn_fp,
		TokenCert:                   tkn_cert,
	}
	return &certGroup, nil
}

func (c TokenCertGroup) GetRsaKey() (*rsa.PrivateKey, error) {
	var rsaKey *rsa.PrivateKey
	if key, ok := c.TokenCert.PrivateKey.(*rsa.PrivateKey); ok {
		rsaKey = key
	} else {
		return nil, errors.New("private key is not a valid rsa key")
	}

	return rsaKey, nil
}
