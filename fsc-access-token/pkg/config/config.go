package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/pkg/errors"
)

type Config struct {
	GrantHash string `json:"grant_hash,omitempty"`

	InwayPeerID string `json:"inway_peer_id,omitempty"`

	OutwayPeerID  string `json:"outway_peer_id,omitempty"`
	OutwayTlsCert string `json:"outway_tls_cert,omitempty"`

	TokenSignCert string `json:"token_sign_cert,omitempty"`
	TokenSignKey  string `json:"token_sign_key,omitempty"`

	ServiceName    string `json:"service_name,omitempty"`
	ServiceAddress string `json:"service_address,omitempty"`

	SignatureAlgorithm string `json:"signature_algorithm,omitempty"`
	TokenLifetime      *int   `json:"token_lifetime,omitempty"`
}

func ReadConfig(configFile string) (*Config, error) {
	configData, err := os.ReadFile(configFile)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read config file")
	}

	var config Config
	err = json.Unmarshal(configData, &config)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse config file")
	}

	return &config, nil
}

func (c *Config) IsValidForToken() bool {
	validationErrors := c.validForToken()
	err := formatValidationError(validationErrors)
	if err != nil {
		log.Println(err)
		return false
	} else {
		return true
	}
}

func (c *Config) validForToken() []string {

	validationErrors := make([]string, 0, 10)
	if c.GrantHash == "" {
		validationErrors = append(validationErrors, "missing: grant_hash")
	}
	if c.OutwayTlsCert == "" {
		validationErrors = append(validationErrors, "missing: outway_tls_cert")
	}
	if c.TokenSignCert == "" {
		validationErrors = append(validationErrors, "missing: token_sign_cert")
	}
	if c.TokenSignKey == "" {
		validationErrors = append(validationErrors, "missing: token_sign_key")
	}
	if c.OutwayPeerID == "" {
		validationErrors = append(validationErrors, "missing: outway_peer_id")
	}
	if c.ServiceName == "" {
		validationErrors = append(validationErrors, "missing: service_name")
	}
	if c.ServiceAddress == "" {
		validationErrors = append(validationErrors, "missing: service_address")
	}
	if c.InwayPeerID == "" {
		validationErrors = append(validationErrors, "missing: inway_peer_id")
	}
	if c.SignatureAlgorithm == "" {
		validationErrors = append(validationErrors, "missing: signature_algorithm")
	} else if !contains([]string{"RS256", "RS384", "RS512", "ES256", "ES384", "ES512"}, c.SignatureAlgorithm) {
		validationErrors = append(validationErrors, "invalid: signature_algorithm")
	}

	if c.TokenLifetime == nil {
		validationErrors = append(validationErrors, "missing: token_lifetime")
	}
	return validationErrors
}

func (cfg *Config) SignatureAlgorithmType() string {
	switch cfg.SignatureAlgorithm {
	case "RS256":
		fallthrough
	case "RS384":
		fallthrough
	case "RS512":
		return "RSA"
	case "ES256":
		fallthrough
	case "ES384":
		fallthrough
	case "ES512":
		return "ECDSA"
	default:
		panic("Unkown signature algorithm")
	}
}

func formatValidationError(errs []string) error {

	if len(errs) == 0 {
		return nil
	} else {
		errorMessage := fmt.Sprintln("Configuration is not valid for the following items: ", strings.Join(errs, ","))
		return errors.New(errorMessage)
	}
}

func contains[T comparable](arr []T, item T) bool {
	for _, v := range arr {
		if v == item {
			return true
		}
	}
	return false
}
