package main

import (
	"fmt"
	"log"

	"digilab.network/gopkg/fsc-validator/fsc-access-token/pkg/accesstoken"
	"digilab.network/gopkg/fsc-validator/fsc-access-token/pkg/config"
	"github.com/alexflint/go-arg"
)

var args struct {
	Config string `arg:"required" help:"path to configuration file"`
}

func main() {
	arg.MustParse(&args)

	configFile := args.Config

	cfg, err := config.ReadConfig(configFile)
	if err != nil {
		log.Fatalln(err)
	}

	if !cfg.IsValidForToken() {
		return
	}

	tokenString, err := accesstoken.CreateTokenString(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Token: ", tokenString)

}
