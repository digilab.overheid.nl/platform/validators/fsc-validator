# FSC Test

## fsc-access-token

### Binary releases

Binary releases can be downloaded here: 
https://gitlab.com/digilab.overheid.nl/platform/validators/fsc-validator/-/releases

```sh
chmod +x fsc-access-token-amd64-linux
./fsc-access-token-amd64-linux --config ./config/fsc/nlx_token.json
```

### Docker images
The configuration and pem files should be accessible for uid:1000 gid:1000

```sh
docker run \
    -v "$(pwd)/config:/config" \
    registry.gitlab.com/digilab.overheid.nl/platform/validators/fsc-validator/fsc-access-token:latest \
    --config /config/fsc/nlx_token.json
```

### Configuration

```json
{
    // Grant hash provided by the inway peer manager
    "grant_hash": "$1$4$QqUPRPuGvnTlVVWQwXlskkRN3C0f2KAMeDTQi5_kk-XLKIJRvz-US3232T5vQmBlbSVO1pi2Xvvqo5Kh8JRs2A==", 
    // ID for the peer the token will be sent to
    "inway_peer_id": "12345678901234567891", 
    // ID for the peer the token will be sent from
    "outway_peer_id": "12345678901234567890", 
    // path to cert pem that will be used to setup tls connection
    "outway_tls_cert": "./config/pki/tls/cert.pem", 
    // path to cert pem that is part of the jwt signing keypair
    "token_sign_cert": "./config/pki/sign/cert.pem", 
    // path to key pem that is part of the jwt signing keypair
    "token_sign_key": "./config/pki/sign/key.pem", 
    // Name of the service that has been registered in the inway peer manager
    "service_name": "Test", 
    // address of the service
    "service_address": "target.local:443", 
    // "RS256" | "RS384" | "RS512" | "ES256" | "ES384" | "ES512"
    "signature_algorithm": "RS512", 
    // Lifetime of token in seconds
    "token_lifetime": 300 
}
```

## fsc-outway

### Configuration

```json
{
    // Grant hash provided by the inway peer manager
    "grant_hash": "$1$4$QqUPRPuGvnTlVVWQwXlskkRN3C0f2KAMeDTQi5_kk-XLKIJRvz-US3232T5vQmBlbSVO1pi2Xvvqo5Kh8JRs2A==", 
    // path to ca pem that will be used to setup tls connection
    "outway_tls_ca": "./config/pki/tls/ca.pem",
    // path to cert pem that will be used to setup tls connection
    "outway_tls_cert": "./config/pki/tls/cert.pem", 
    // path to key pem for setting up the tls connection
    "outway_tls_key": "./config/pki/tls/key.pem",
    // inway url to connect to: https://<host>:<port>
    "inway_url": "https://inway.local",
}
```

## Development
### Traefik
Download the latest traefik release from `https://github.com/traefik/traefik/releases` and place it somewhere in your `$PATH`
(Tested with version v2.10.1)

### PKI
Download the `pki` folder from nlx the nlx repo `https://gitlab.com/commonground/nlx/nlx` to `./config/pki`
(Tested with version git `25c01c25fdb6b1599ab5797a66de89166036242b`)

## Development

- start the traefik inway with `make run`
- start the service emulator with `make service` or `make service2`
- build a token using `make token`
- build a token and send a request to the inway using `make outway`

