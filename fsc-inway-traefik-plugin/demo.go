package fsc_inway_traefik_plugin

import (
	"context"
	"crypto/rsa"
	"fmt"
	"net/http"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/certutil"
	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/jwtutil"
	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/util"
)

type Config struct {
	TokenCertFile string                `json:"tokenCertFile"`
	Peers         map[string]PeerConfig `json:"peers"`
}

type PeerConfig struct {
	TokenCertFile string   `json:"tokenCertFile"`
	Services      []string `json:"services"`
}

type Peer struct {
	TokenCertPublicKey   rsa.PublicKey
	TokenCertFingerprint string
	Services             map[string]bool
}

type PeerSet map[string]Peer

func (p *Peer) String() string {
	return fmt.Sprintf("FP: %s SVCS: %+v", p.TokenCertFingerprint, p.Services)
}

func CreateConfig() *Config {
	return &Config{
		TokenCertFile: "",
		Peers:         make(map[string]PeerConfig),
	}
}

type Demo struct {
	next                 http.Handler
	tokenCertPublicKey   rsa.PublicKey
	tokenCertFingerprint string
	peers                PeerSet
}

func New(ctx context.Context, next http.Handler, config *Config, name string) (http.Handler, error) {
	if len(config.TokenCertFile) == 0 {
		return nil, fmt.Errorf("certificate path cannot be empty")
	}
	util.Print("--------------------------------------->\n")

	if len(config.Peers) == 0 {
		return nil, fmt.Errorf("peer config cannot be empty")
	}

	peers := make(PeerSet)
	for peerId, peerConfig := range config.Peers {
		cert, err := certutil.LoadX509Cert(peerConfig.TokenCertFile)
		if err != nil {
			return nil, fmt.Errorf("failed to read certificate from %s, %v", config.TokenCertFile, err)
		}

		tokenCertFingerprint := certutil.CertificateFingerprint(&cert)
		rsaPublicKey := cert.PublicKey.(*rsa.PublicKey)

		peer := Peer{
			TokenCertPublicKey:   *rsaPublicKey,
			TokenCertFingerprint: tokenCertFingerprint,
			Services:             make(map[string]bool),
		}

		for _, svc := range peerConfig.Services {
			peer.Services[svc] = true
		}

		peers[peerId] = peer
	}

	for k, v := range peers {
		util.Print("PEER K: %s, V: `%s`\n", k, v.String())
	}

	cert, err := certutil.LoadX509Cert(config.TokenCertFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read certificate from %s, %v", config.TokenCertFile, err)
	}

	tokenCertFingerprint := certutil.CertificateFingerprint(&cert)
	rsaPublicKey := cert.PublicKey.(*rsa.PublicKey)

	demo := &Demo{
		next:                 next,
		tokenCertPublicKey:   *rsaPublicKey,
		tokenCertFingerprint: tokenCertFingerprint,
		peers:                peers,
	}
	return demo, nil
}

func (a *Demo) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	jwtHeader := req.Header.Get("fsc-authorization")
	// grant := req.Header.Get("fsc-grant-hash")
	// transaction := req.Header.Get("fsc-transaction-id")
	// request_org := req.Header.Get("x-nlx-request-organization")

	peerCertificateFingerprints := make(map[string]bool)
	for _, cert := range req.TLS.PeerCertificates {
		fp := certutil.CertificateFingerprint(cert)
		peerCertificateFingerprints[fp] = true
	}

	payload, err := jwtutil.ParseJwtHeader(jwtHeader, &a.tokenCertPublicKey, a.tokenCertFingerprint, peerCertificateFingerprints)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	util.Print(payload.String())

	util.Print("Adding service header: %s", payload.ServiceName)
	req.Header.Add("X-Nlx-Service", payload.ServiceName)

	a.next.ServeHTTP(rw, req)
}
