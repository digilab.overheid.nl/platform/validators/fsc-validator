module digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin

go 1.19

require (
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/pkg/errors v0.9.1
)
