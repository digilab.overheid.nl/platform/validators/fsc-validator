package util

import (
	"fmt"
	"os"
)

func Print(format string, a ...any) {
	output := fmt.Sprintf(format, a...)
	os.Stdout.WriteString(output)
}
