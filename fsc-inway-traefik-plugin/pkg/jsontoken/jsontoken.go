package jsontoken

import (
	"fmt"

	"github.com/golang-jwt/jwt/v5"
)

type JsonToken struct {
	GrantHash             string             `json:"gth,omitempty"`
	OutwayPeerID          string             `json:"sub,omitempty"`
	OutwayDelegatorPeerID string             `json:"cdi,omitempty"`
	ServiceName           string             `json:"svc,omitempty"`
	ServiceInwayAddress   string             `json:"aud,omitempty"`
	ServicePeerID         string             `json:"iss,omitempty"`
	ServiceDelegatorID    string             `json:"pdi,omitempty"`
	ExpiryDate            int64              `json:"exp,omitempty"`
	NotBefore             int64              `json:"nbf,omitempty"`
	ConfirmationMethod    ConfirmationMethod `json:"cnf,omitempty"`
}

type ConfirmationMethod struct {
	Fingerprint string `json:"x5t#S256,omitempty"`
}

func (t JsonToken) String() string {
	return fmt.Sprintf(`JsonToken:
	GrantHash: %s
	OutwayPeerID: %s
	OutwayDelegatorPeerID: %s
	ServiceName: %s
	ServiceInwayAddress: %s
	ServicePeerID: %s
	ServiceDelegatorID: %s
	ExpiryDate: %d
	NotBefore: %d
	ConfirmationMethod:
		Thumbprint: %s
`,
		t.GrantHash,
		t.OutwayPeerID,
		t.OutwayDelegatorPeerID,
		t.ServiceName,
		t.ServiceInwayAddress,
		t.ServicePeerID,
		t.ServiceDelegatorID,
		t.ExpiryDate,
		t.NotBefore,
		t.ConfirmationMethod.Fingerprint,
	)
}

func (t JsonToken) GetExpirationTime() (*jwt.NumericDate, error) {
	return nil, nil
}
func (t JsonToken) GetIssuedAt() (*jwt.NumericDate, error) {
	return nil, nil
}
func (t JsonToken) GetNotBefore() (*jwt.NumericDate, error) {
	return nil, nil
}
func (t JsonToken) GetIssuer() (string, error) {
	return "", nil
}
func (t JsonToken) GetSubject() (string, error) {
	return "", nil
}
func (t JsonToken) GetAudience() (jwt.ClaimStrings, error) {
	return nil, nil
}
