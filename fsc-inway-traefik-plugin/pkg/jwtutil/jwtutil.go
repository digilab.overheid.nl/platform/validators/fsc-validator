package jwtutil

import (
	"crypto/rsa"

	jwt "github.com/golang-jwt/jwt/v5"
	"github.com/pkg/errors"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/jsontoken"
	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/util"
)

const X5THeader = "x5t#S256"

// Traefik's go interpreter Yeagi has issues with json decoding (https://github.com/traefik/yaegi/issues/1486)
// Because of this we use jwt.MapsClaims, and then convert to our own type
func ParseJwtHeader(tokenString string, key *rsa.PublicKey, tokenCertFingerprint string, peerPublicKeyFingerprints map[string]bool) (*jsontoken.JsonToken, error) {
	token, err := jwt.ParseWithClaims(tokenString, jwt.MapClaims{}, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})

	map_claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid || err != nil {
		return nil, errors.Wrap(err, "invalid token")
	}

	json_claims, err := mapClaimsToJsonToken(map_claims)
	if err != nil {
		return nil, errors.Wrap(err, "invalid token")
	}

	x5tHeaderValue, ok := token.Header[X5THeader].(string)
	if !ok {
		return nil, errors.New("missing x5t#S256 from jwt header")
	}
	if x5tHeaderValue != tokenCertFingerprint {
		return nil, errors.New("x5t#S256 from jwt header is not valid")
	}
	util.Print("X.509 Certificate SHA-256 Thumbprint ok: %s\n", x5tHeaderValue)

	cert, ok := peerPublicKeyFingerprints[json_claims.ConfirmationMethod.Fingerprint]
	if !cert || !ok {
		return nil, errors.New("x5t#S256 from cnf is not valid")
	}
	util.Print("Confirmation methond thumbprint ok: %s\n", json_claims.ConfirmationMethod.Fingerprint)

	return json_claims, nil
}

func mapClaimsToJsonToken(claims jwt.MapClaims) (*jsontoken.JsonToken, error) {
	exp, err := claims.GetExpirationTime()
	if err != nil {
		return nil, errors.New("invalid exp")
	}

	nbf, err := claims.GetNotBefore()
	if err != nil {
		return nil, errors.New("invalid nbf")
	}

	aud, err := claims.GetAudience()
	if err != nil {
		return nil, errors.New("invalid aud")
	}

	iss, err := claims.GetIssuer()
	if err != nil {
		return nil, errors.New("invalid iss")
	}

	sub, err := claims.GetSubject()
	if err != nil {
		return nil, errors.New("invalid sub")
	}

	gth, err := getByKey[string](claims, "gth")
	if err != nil {
		return nil, errors.New("invalid gth")
	}

	cdi, err := getByKey[string](claims, "cdi")
	if err != nil {
		return nil, errors.New("invalid cdi")
	}

	svc, err := getByKey[string](claims, "svc")
	if err != nil {
		return nil, errors.New("invalid svc")
	}

	pdi, err := getByKey[string](claims, "pdi")
	if err != nil {
		return nil, errors.New("invalid pdi")
	}

	cnf, err := getByKey[map[string]interface{}](claims, "cnf")
	if err != nil {
		return nil, errors.New("invalid cnf")
	}

	thumb, err := getByKey[string](cnf, X5THeader)
	if err != nil {
		return nil, errors.New("invalid x5t in cnf")
	}

	return &jsontoken.JsonToken{
		GrantHash:             gth,
		OutwayPeerID:          sub,
		OutwayDelegatorPeerID: cdi,
		ServiceName:           svc,
		ServiceInwayAddress:   aud[0],
		ServicePeerID:         iss,
		ServiceDelegatorID:    pdi,
		ExpiryDate:            exp.Unix(),
		NotBefore:             nbf.Unix(),
		ConfirmationMethod: jsontoken.ConfirmationMethod{
			Fingerprint: thumb,
		},
	}, nil
}

func getByKey[T any](m map[string]interface{}, key string) (T, error) {
	var value T

	raw, ok := m[key]
	if !ok {
		return value, nil
	}

	value, ok = raw.(T)
	if !ok {
		return value, jwt.ErrInvalidType
	}

	return value, nil
}
