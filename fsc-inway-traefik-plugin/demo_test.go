package fsc_inway_traefik_plugin_test

import (
	"crypto/rsa"
	"testing"

	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/certutil"
	"digilab.network/gopkg/fsc-validator/fsc-inway-traefik-plugin/pkg/jwtutil"
)

func TestJWT(t *testing.T) {

	// Prepare token token_cert data
	token_cert, err := certutil.X509Cert([]byte(token_cert))
	if err != nil {
		t.Errorf("Failed to parse token cert %v", err)
	}
	tokenCertFingerprint := certutil.CertificateFingerprint(&token_cert)
	tokenRsaPublicKey := token_cert.PublicKey.(*rsa.PublicKey)

	// Prepare tls cert data
	org_cert, err := certutil.X509Cert([]byte(org_cert))
	if err != nil {
		t.Errorf("Failed to parse org cert %v", err)
	}
	OrgCertificateFingerprint := certutil.CertificateFingerprint(&org_cert)
	peerCertificateFingerprints := make(map[string]bool)
	peerCertificateFingerprints[OrgCertificateFingerprint] = true

	// Use prebuilt token
	jwt_token := "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCIsIng1dCNTMjU2IjoibzA5VHhWTllsTi1sb1labWdqYXVGblR4NDRYYXdGRE9aVm9hUWNCRXRmcz0ifQ.eyJndGgiOiIkMSQ0JFFxVVBSUHVHdm5UbFZWV1F3WGxza2tSTjNDMGYyS0FNZURUUWk1X2trLVhMS0lKUnZ6LVVTMzIzMlQ1dlFtQmxiU1ZPMXBpMlh2dnFvNUtoOEpSczJBPT0iLCJzdWIiOiIxMjM0NTY3ODkwMTIzNDU2Nzg5MCIsInN2YyI6IlRlc3QiLCJhdWQiOiJpbndheS5vcmdhbml6YXRpb24tYi5ubHgubG9jYWw6NDQzIiwiaXNzIjoiMTIzNDU2Nzg5MDEyMzQ1Njc4OTEiLCJleHAiOjE2ODQ4NTI0MTksIm5iZiI6MTY4NDg1MjExOSwiY25mIjp7Ing1dCNTMjU2IjoibVJWby1oVDhDX0VRV0lJV0dqMl92cnIzZEtHalM1bjk5d1FiOVAxRHFvND0ifX0.ng9st7E1ZaT_z-eN_DAx7cJMZ1ZUJvyqSyWmqSqtqdHbbMMJaKqXQ6UmpCXdU1WXLFDJJGu45RfHuTCMUaB-Wd1sH4w5CiYvQoSBGq-qWeFzgpAnC-HZn4PwRZMzBzbYNIQa62Q-ip1khRuw_HPq7f_7IrWbVV_xgXXuRB-YAwJMi0iXAwMT1PbTm8uIpPytp1qMEX6Vdb3I1pCrA9kfJ83kTozy75-dKzkOTDb1eFlmxohi4LH95SUzEn6-i9zBz2bOX0uzbyS_M6IyC1XbtmKdrDyCZuQLXt_C67RdY0TCETbsPUAPtSqUBH-ljM3_gvGvltaugt22JbdlB3fr-FcFqYxz21_49SR97k6lzohk3IAED7gEYoxuGbtYjaXnvitpnocQFIHGRkTGSNWk3CrWMr54-TJttrqL0u6-3tVC61aADJ4MvEaBLqI3EtuUSmyMIlvMYbYnqrxACoYhm_WFLm-55X-Fj09SUNW9aDrmg-iROyb6vcwja8tYiozW-kUnrZM3aghWqZBiRfD4rrXzH6d0hiTZppVRiNSRtGPR8giwjKtQ2voOVKaZTo1bjjwByciq_LoxkBxvlLq43v_VUkLVsLp7pYXJk6oyVzRMj0-rYpkEGwwzjeI9ZPEzh1xhB8ez4V_ugpm1J49mXbln_nmtSy6UR6807smWy_A"

	// Parse token
	claims, err := jwtutil.ParseJwtHeader(jwt_token, tokenRsaPublicKey, tokenCertFingerprint, peerCertificateFingerprints)

	// Will fail due to expired token
	if err != nil {
		t.Errorf("Jwt error %v", err)
	}

	assertEq(t, claims.ServiceName, "Test")

}

func assertEq[T comparable](t *testing.T, actual T, expected T) {
	if actual != expected {
		t.Errorf("Expected %v, actual %v\n", expected, actual)
	}
}

const token_cert = `-----BEGIN CERTIFICATE-----
MIIF3DCCA8SgAwIBAgIUCpblBfOUVdLxqIHTxGtH5wa+cPUwDQYJKoZIhvcNAQEN
BQAwOzELMAkGA1UEBhMCTkwxDDAKBgNVBAoTA05MWDEeMBwGA1UEAxMVTkxYIElu
dGVybWVkaWF0ZSBDQSAxMB4XDTIzMDIxNzEyNDIwMFoXDTI2MDIxNjEyNDIwMFow
ZTELMAkGA1UEBhMCTkwxDTALBgNVBAoTBFJ2UkQxKDAmBgNVBAMTH21hbmFnZXIu
b3JnYW5pemF0aW9uLWIubmx4Lm9jYWwxHTAbBgNVBAUTFDEyMzQ1Njc4OTAxMjM0
NTY3ODkxMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAt07z/+w+3zB1
gRsnu85XKY0M0t4aU1vtzpmz4sEhaJkR1QUvahuChwt6QY2+QDjzfBqddczl7/el
xMU3l5USJfgqXwFQ3GmL8wXI5ZQJ9EZSj1rx4RHaq7RLxvZryK+mTgx5QHTasoKG
m3ZrShSod+JhO1NWFw4ZQBisnzOm+uWAsQ6PD9f9tPKr0XuD6cItHz9B7GnQlNKI
mdq3MJWYgb1Fq7mvuLLRgyeqFSSAWdrOZQqTBY1uklwBdJD5QtrQ/AOHIjopk0ui
yX00KHOPOtfzCRcxWWkf/qJEu3RQzxDIYMzH1G//FAUr1mfCbGpLTjcgeWuZ91nD
VsxN/GI5fM+zaqVN4wvfWbJrEyDmzlWQaLtZJGU85LLmsnojxl4139sgAvdJ+D1r
MUGOMSodwoGPCEXPmm/Ns+sAxFu3uW8GiMrnBKC6/Oksigu9XElDXpY3kJ5MqYVL
DZnhobOt+3RAntAEJawpwGNDF0rt5jcK9xKikNHqC8M8x7w5XOK2wG9rwM6RhW7d
GbFMCPI6FsIuJf+SD9R/WDpwjTAMIDsewDosfsLzOV8hgoCLsRzKqIhSSaLfH68w
0KAdSEgVatoySm/X/VvTuhR6BWI9nL5nEfIcvqiX+kdmSwNRnYBNm/B7NBljk5QX
kbYJs8rhfK+CU6awVkbMLyeLvUHJWCECAwEAAaOBrTCBqjAOBgNVHQ8BAf8EBAMC
BaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAw
HQYDVR0OBBYEFFx9w3KhzmZwMXgN2G7PlD2I5C5PMB8GA1UdIwQYMBaAFIlN8AIy
sXb9rsSAaB7IbwuT/263MCsGA1UdEQQkMCKCIG1hbmFnZXIub3JnYW5pemF0aW9u
LWIubmx4LmxvY2FsMA0GCSqGSIb3DQEBDQUAA4ICAQBX6Qs5hxfgt30ElhDfEtnl
PZjJGn+Qp8bdjkcAJYKfj6Si6qHi87gW055+9I+hoI8xm4oGXF9F0UjJ/p85rhe+
IWDNPCXSr+RD8nICVXjGtsOikVQdjN5dmwB0IDPOIUr5WmTti8K0rySfV1BVB7OC
0DGV7owZGjLaAfiuFTrs6UbzLknc/krskouxu4TEEdFmED/BjkBxWBTh8HB7rjBe
n/qIY7662dIpFcazjH18L8zLoZiYq5ihRIiBo9SyfQCja3ZKHtTEGrBj3hDWjcxf
s01Lp7ooZV7HbCfvZ71FnvJ9alhN2bGUF58vKBQipzN6izcLl90L3ZO+EOllklVA
yZBcFG8MFwRzwMvLyRShCMlOjPUFOVPN6gzwPvHzgFauFE6djBnGqCHh0KIFyLg+
q0Spt2jCQj6SjcYxCyAvNV8b+JjQwp17vlWPTDGStUFmoZKPVlilmNYoazFTq42a
jRO7rp8bzSNaPPIaBRppttRoORbRRmWhoPKFHdRQbD3EUSlPpOgGlfSgnuhz22yL
ol05w6A6dweMzR6GXSrGlVy8lYryx96ZkEBWS8/r0T7rS8lONvCY2OKMlJ/ZP+jX
ahwPRFqyGMZJ5iYETUSLULbNQCagNHEZEdHmHYHAj3kuXrl7DfQwLK0yve4CM7EM
qmm1lFT97cxbY3MhSbxP9Q==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFYjCCA0qgAwIBAgIUTWv8x9tjVOKOmP3/o6Bo3ZhaAEIwDQYJKoZIhvcNAQEN
BQAwMzELMAkGA1UEBhMCTkwxDDAKBgNVBAoTA05MWDEWMBQGA1UEAxMNTkxYIFJv
b3QgQ0EgMTAeFw0yMzAyMTQxMjUzMDBaFw0zODAyMDkxMjUzMDBaMDsxCzAJBgNV
BAYTAk5MMQwwCgYDVQQKEwNOTFgxHjAcBgNVBAMTFU5MWCBJbnRlcm1lZGlhdGUg
Q0EgMTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAKal73Z4PY0tWZuQ
d2iTcr3Pf1Am8a9rSo9ZtzWAx/aeLcsRPaBKYlNys1RoXGJ7euijYJfGtLTIJRYU
Pfud0FDs7J5l6kiM8bJUYrpxRRCRxCVd+q+Is+TQUIbQ52zd6vlDTbPhPzBQHcQf
YrkKSaLHqmagNIut+zLAGKpLA8FCK0cHj7i5k4eoMwKDvTRks7URiCbEZTVAOomV
mg9lW2BTLL82ppfGO2u/SJEEMMYsqZoO4jfO22MaNa/Gar8mQwT+IyaVp30j71Yq
mrhCZxVP2R1LGHlN6uoV/iVB7tAqZuBDQJsWuLeANOm9LDgmN3zwm1ofDtpTAvoa
B4WIo559IQoG2t9iYbrBiYjhw506RJkgser2klkTWFy4qy3KCpXmXRVXThiyjG+g
zCJ+o6m9bwMMlwp3WqRlzI3gETFIS54fFQAGKjTdoutWPMerHPc2O/DVIShzK58v
AJ2cD9tkQZKXldl4by1oqew+lxG3HfFivkW+8bOwg5vnjWFUJ/F6ehnqPazibN7Z
OEELgSpzqtjing2xuVaORnDNg3P1Ml84cobNLEAH33R2N1vnCU1YyUVMsNdnUPsi
zQXPMfECw//JlXF6LSJ/KDRBWlEzfz0eNNMQu4KLl0c9gWvoUGd8NuV7TMsCvdIa
OP3gGvJiQY/4TqowWlIK1G/eFszdAgMBAAGjZjBkMA4GA1UdDwEB/wQEAwIBhjAS
BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBSJTfACMrF2/a7EgGgeyG8Lk/9u
tzAfBgNVHSMEGDAWgBR7ff9ZwRVg+0alwzslynbmrnDeljANBgkqhkiG9w0BAQ0F
AAOCAgEAICudci0svRnKtlZbcgvvqk6U4ecedpG1n0AVKuA44jrWFcPmgb4Apo1l
1hpauyXAx4I7QlqxVNXA4C7d34hOykwZ7J9mLSYDp9lz+WQFz1+YVNTNjGxvZFca
iwgTb3blRe0VgX1T+2N/BH4ai6kROjQF4Q5pq9pdNitslzU26vDxDOcHPOw+SUSF
SeBDTUSpDJlZdfcm0lcrabPFx3g/7CPME9cIdYDhajCj4slx6hJ4F2lrjuqD9FXP
m6MFTMmrJl9yPCioA5qIZeixJ/pXTeVs8nTK9mqLP+3EPG5mLiOyUEYy5G3jg647
7Gico75pMCYqfb6PglGUtzk5CLyk+3oB9Z5Z46sYUfGf4DxK2LzgdKDko3BhbyuX
SsUeS/Pw6zuZ7ceTqIk3TtVbmQ0uZAlot0xLmSG7CJBe14onPdG8er5TfBHixgBP
b9KGxiSRC2FV/E6je2/PrMf1tsbqA1znaDiQIENHKJJBGmVnjoqQj6BOGThXWtUZ
Yd5LKI96VLduyT8YhOF//2J2H9B0q58UUjWLHccIy11ioeIkPPyO/OlgBAqMkjSm
xIBkCmyMqpFI1CJLOjb5/Rd6/meNrFgRmSEm2t1f/12wL+tlvKrDMPMgNSloXKQn
9jBmVZalCQXOpdfR39OmOxJDtzkeaeXJDaXDAtDfibQRp6QbF3M=
-----END CERTIFICATE-----
`

const org_cert = `-----BEGIN CERTIFICATE-----
MIIF6jCCA9KgAwIBAgIUDhK39+6zYLu1i+ylz+TvGQEiQMkwDQYJKoZIhvcNAQEN
BQAwOzELMAkGA1UEBhMCTkwxDDAKBgNVBAoTA05MWDEeMBwGA1UEAxMVTkxYIElu
dGVybWVkaWF0ZSBDQSAyMB4XDTIzMDIxNzEyNDIwMFoXDTI2MDIxNjEyNDIwMFow
cjELMAkGA1UEBhMCTkwxGDAWBgNVBAoTD0dlbWVlbnRlIFN0aWpuczEqMCgGA1UE
AxMhb3V0d2F5LTIub3JnYW5pemF0aW9uLWEubmx4LmxvY2FsMR0wGwYDVQQFExQx
MjM0NTY3ODkwMTIzNDU2Nzg5MDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoC
ggIBAMkSXoXHuSXmhgqx17L2JWQHUHBSWMMzKoDTmNChMcPVMa/OSX2FDhEnRrTl
V1OFZ16PCutAgmNrew4GmdK9P9+gwEo3u+G1BPY1WOCd/OWazHpeGK+xHhrHEMKY
76ljpcvQXloJMdB6mS+SPJU/hDloF602ufVVMdYmljjlVZhRGtSRx/hqD/8i6Qgd
xeReQjuw83GfyILWmNv8bcurrB28Nf/ymNhGnQ9N7AOPfH0pkzVbrIDbzLEALqYf
3NVJ5PkfwiPL95Y/p6GmRBjtCjuWrgi8AB0pKaqZY9h1+4F37N71WoKw/ZAPGwUb
dD1SqQTgYBuseEamfHgadk8/RNG1xT71Abir//XAijP+rA/MN6Wk6+GE3QURfAZ9
UtF3VJNMrhPcTAz/egr3+DtFMChr8msov8rupibFG9PsjXnhTWhGsVcyc8X2spNr
82b3daUOkuibKSmGGEeRpHdTl8o5thMP4Cf1SvHSObIJMV9U5gE/18rDH5EOr4KL
GgW7MSrIoCSrqYhBz/Qh43PjasCgTFRZJcV2C0ocDdb30mgId10b8fdrvll+vC4f
aDWe/znlZVYM1rBCWR8PY/+VewbTZWlCiWs/GWP9SILQOVSDwFoVmuXddKyM4Mnu
fxUWCKDfmAZtKnCtjqAdzdMDDGtaJ8Hopham+mVvWNG+emWjAgMBAAGjga4wgasw
DgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAM
BgNVHRMBAf8EAjAAMB0GA1UdDgQWBBSYn6ADfRMY2SDDqsZJXQJdgSMHEDAfBgNV
HSMEGDAWgBRslmlb5mfvpGpGrHLuTSPH1Od03zAsBgNVHREEJTAjgiFvdXR3YXkt
Mi5vcmdhbml6YXRpb24tYS5ubHgubG9jYWwwDQYJKoZIhvcNAQENBQADggIBAEfx
GEq07QK2KCoaxLBjAkbLMgq/mFCCCNX06IKGPw+04GgmmMyij/t8btJVWzxOOUzm
J9Zjf/IKra1wNQoQsYppysQJ8lhRfDUfdbkBaf4meJJi8NqxZBAtSbD+RTdkYYoS
qRYpxISR0kwjXsMN0S7hnYGOaVncYaJFAPMROLBBmGnLkP2U4uBnDTbSub20JzUr
HiZ+BmncuqoSA+i8BPI0/lm0dKFnZ0ybmLp5p/qiR1Je2QuYBvfir1MH7BKUJ6lj
1nheLXe92MwyA2HF3T51inG50ysdoW6tv91PSlDavEQnMDoVDxwNDUNqf4OSl4WV
YSfDfaKNg1J07eBW+tseC/NPKrYTKaNjkxxbnexrTVOMKLa1IPaODGfivYN/TzPc
SpsGLURlacryw99lZfA+rAP8ZRw1x/uol8+HZqYxtHKPDzDvg2ZJ8gu9zVg1oKOh
o3QCkUOkwHVESeTWqviq6VuCgBOQwEvk46Uyj7kNMh/6Q4LPC2bgcGvBXB43YseZ
TL/g8qgXTb+bv3K/V7YiYekIWFeYK0wB7padDjt4z7TC8wOYqfJfAuH+c7Qux+H9
oN6xCfPwBMIBunSYqjL/skOs4AsJCdUHuwEt+LBekwF7GWCdrXPlj3a2Wnoo3kcf
l4KkIsYU16yMbfNGAP3VxnuDlXOurQOjD/F4gH6U
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFYjCCA0qgAwIBAgIUEBebQghi+0FttdD64irrHGWOC/4wDQYJKoZIhvcNAQEN
BQAwMzELMAkGA1UEBhMCTkwxDDAKBgNVBAoTA05MWDEWMBQGA1UEAxMNTkxYIFJv
b3QgQ0EgMjAeFw0yMzAyMTQxMjUzMDBaFw0zODAyMDkxMjUzMDBaMDsxCzAJBgNV
BAYTAk5MMQwwCgYDVQQKEwNOTFgxHjAcBgNVBAMTFU5MWCBJbnRlcm1lZGlhdGUg
Q0EgMjCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANpLGFMXXi+WPXU4
Efm6wsDzuPPiS915eFEMgcmvo+UQpzJ+fAwVJeBDtXkt7C2G6JUKmjMVpUSK8ZZf
xXJotKmPBITyZjfATtFxHF7MJqUVzIjHRO4CTnxOAMpK2mSmYoAGoj6UiBddnAbK
NKWxhPgQqrekIp8Xx4Suo4aiM4PF5Ky7syE+uXaho1HMS9DtgM7zAb1Dehf6/KKK
lGICPLgenT+vWwzxtcVU/XK1on4A+QinPDJlnZoEx/n2U8qOMc0MnakfB0tSjbIm
DKQipdIXzzOcNovpWl8nAzCWFJ78nUxBxd08PsgPiromT+M18E7MqRZJSeKS5LoZ
Ec5U1l7i+pglSiM1H3Y9/lUSTOGwcx0MR6/1q3tgEMYedyY/SBLOVGjAQ87lHREA
F17hXiwS091J/4FWBMYJQN13sN457ADhEqA0v+JsbrSylzWW9LzgiAj08Kb/v01Q
kNDqDtPM+ex5XWoGXbge6CcdY01RF8800XGV8H/3/vFxLlVXGg2B7sXrbpRNCxR/
hs1Y+24Ff8J6YxDXhmoylzI2Hx98sr2OokxiTbMrELYRyJZZ3XftB4RMXnsnkPs4
H7oJotkbKO8g0U0ZbFKpG5ajPjnpxtUI0CpyKjTg/eyhA9UqKgA3idhdlsHe06PQ
oIlraJgiLD+/tmy18xVCmvzRWCuvAgMBAAGjZjBkMA4GA1UdDwEB/wQEAwIBhjAS
BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBRslmlb5mfvpGpGrHLuTSPH1Od0
3zAfBgNVHSMEGDAWgBQ5PohkIPaOod9cBbq0g5RTPrEvXDANBgkqhkiG9w0BAQ0F
AAOCAgEAh+ybQxbgcwEYx87C0CfNuBwwCDC55BUuYJktM5RizcnzXOITxVZoVqTE
ZkBmgubXu2GazU10EpnxG+f9r4ib7OSZsFq+aPZi6sv3Jn3z2aNgHAQrCDO7EkoN
H3+gbVMHN+8cPFysQG/krazIMCFVDbbg94kZf6v/8sT/szxq5PUrlURk3x9mJixL
7OAzab4YP82/mmuYC5Bh2M7BH8yHCP7/cDhE1IwB4Yt6izOVBEcp+Y/Z2JyY9b2w
+ayTu+ZlWqy92jGFyZSt7Vvhlw7gkIcqY2zWferr+pHBUPDp4ypDQvetUCaieB4J
CRl4+MgGszLcMshG2CbdgVqAUO+VUtkfFiTnDPG5xIN7j4gNGRSbVx2e4/nSAr/T
u6Rc6UNTkYuCKe0YzkT6bTZ45MFCBVhvOOB9kXrW90/U2HR1LPz4F8V2PvvnEmrv
DmCd9fD7TfWsTIFiE6jXSK8n38UZq3J71wQk/0BeD/y5nAwbcnuDOBYwF/Hzq6nd
NYODX5Jx5+7GHkKWgUe5gTVv/z3FFdPRrxYfl2+O6pgiCSkGqvbCEj1xSzONnHWC
TtnOEgid/SuHHBg1pW97MbO8ZT2ThtMJ/n8aO78V8oH4wGQnZzdsINw3GwX4lTt3
Nr9kx+Zo4obfv2INduO2eMB9Ao6kDe38HDp1tBRnp5rrOZr1cAk=
-----END CERTIFICATE-----
`
