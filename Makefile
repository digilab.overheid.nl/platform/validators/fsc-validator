# https://github.com/traefik/plugindemo
# https://plugins.traefik.io/install
# https://plugins.traefik.io/create
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: run
run:
	(cd ./config/traefik && traefik --configfile ./config_static.yaml)

.PHONY: service
service:
	go run ./fsc-service/cmd/

.PHONY: service2
service2: 
	go run ./fsc-service/cmd/ --port=22222 --name=Test2

.PHONY: token
token:
	go run ./fsc-access-token/cmd --config "./config/fsc/nlx_token.json"

.PHONY: token-container
token-container:
	docker run \
		-v "${ROOT_DIR}/config:/config" \
		registry.gitlab.com/digilab.overheid.nl/platform/validators/fsc-validator/fsc-access-token:560eb290 \
		--config /config/fsc/nlx_token.json

.PHONY: outway
outway:
	go run ./fsc-access-token/cmd --config "./config/fsc/traefik_token.json"\
	| awk '{print $$2}' \
	| xargs go run ./fsc-outway/cmd/ --config "./config/fsc/traefik_req.json" --tokenstring 

.PHONY: outway-nlx
outway-nlx:
	go run ./fsc-access-token/cmd --config "./config/fsc/nlx_token.json"\
	| awk '{print $$2}' \
	| xargs go run ./fsc-outway/cmd/ --config "./config/fsc/nlx_req.json" --tokenstring 

.PHONY: certscan
certscan:
	go run ./tools/certscan/cmd/ --path "./config/pki"

.PHONY:	dist
dist:
	mkdir -p ./dist
	# Linux
	GOOS=linux GOARCH=amd64 go build -o ./dist/fsc-access-token-amd64-linux ./fsc-access-token/cmd
	# Windows
	GOOS=windows GOARCH=amd64 go build -o ./dist/fsc-access-token-amd64.exe ./fsc-access-token/cmd
	# Macos
	GOOS=darwin GOARCH=amd64 go build -o ./dist/fsc-access-token-amd64-darwin ./fsc-access-token/cmd
	GOOS=darwin GOARCH=arm64 go build -o ./dist/fsc-access-token-arm64-darwin ./fsc-access-token/cmd

